import com.intellij.openapi.application.ApplicationManager
import com.intellij.serviceContainer.ComponentManagerImpl
import liveplugin.PluginUtil
import liveplugin.registerAction
import liveplugin.whenDisposed
import tidal.*

// add-to-classpath $PLUGIN_PATH/build/classes/kotlin
// add-to-classpath $PLUGIN_PATH/build/resources/main

val groupId = "Tidal Cycles"
registerAction("Start Or Stop Tidal", actionGroupId = groupId, action = StartOrStopTidal())
registerAction("Send Line To Tidal", "ctrl L", actionGroupId = groupId, action = SendLineToTidal())
registerAction("Send Paragraph To Tidal", "ctrl K", actionGroupId = groupId, action = SendParagraphToTidal())
registerAction("Hush Tidal", "ctrl H", actionGroupId = groupId, action = HushTidal())

val tidalService = TidalService(
    ghciProcess = PluginUtil.getGlobalVar<Process>("ghciProcess"),
    stopOnDisposal = false
)
(ApplicationManager.getApplication() as ComponentManagerImpl)
    .replaceServiceInstance(TidalService::class.java, tidalService, pluginDisposable)
pluginDisposable.whenDisposed {
    show("Old ghciProcess: ${tidalService.ghciProcess}")
    PluginUtil.setGlobalVar<Process>("ghciProcess", tidalService.ghciProcess)
}
