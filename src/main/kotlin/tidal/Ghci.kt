package tidal

import com.intellij.concurrency.JobScheduler
import java.io.Reader
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit.MILLISECONDS

interface Ghci {
    fun start(createProcess: () -> Process)
    fun writeLine(line: String)
    fun stop()
    fun isRunning(): Boolean
}

class GhciImpl(
    private val onStdOut: (String) -> Unit,
    private val onStdErr: (String) -> Unit,
    private val onInput: (String) -> Unit
) : Ghci {

    private var process: Process? = null
    private var scheduledFuture: ScheduledFuture<*>? = null

    override fun start(createProcess: () -> Process) {
        process = createProcess()
        val outReader = process!!.inputStream.reader()
        val errReader = process!!.errorStream.reader()
        scheduledFuture = JobScheduler.getScheduler().scheduleWithFixedDelay(
            {
                outReader.readString().takeIf { it.isNotBlank() }?.replace("Prelude>", "")?.trim()?.let(onStdOut)
                errReader.readString().takeIf { it.isNotBlank() }?.replace("Prelude>", "")?.trim()?.let(onStdErr)
            },
            0, 100, MILLISECONDS
        )
    }

    override fun writeLine(line: String) {
        onInput(line)
        val writer = process?.outputStream?.writer() ?: return
        writer.write("$line\n")
        writer.flush()
    }

    override fun stop() {
        process?.destroy()
        scheduledFuture?.cancel(true)
    }

    override fun isRunning(): Boolean = process?.isAlive ?: false

    private fun Reader.readString(): String {
        var lines = ""
        while (ready()) {
            lines += read().toChar()
        }
        return lines
    }

}
