package tidal.settings

import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.util.xmlb.XmlSerializerUtil
import java.io.File

@State(name = "Tidal Settings", storages = [Storage("tidal.xml")])
data class Settings(
    var ghciPath: String = "ghci",
    var tidalBootScriptPath: String = "",
    var logEvaluatedCode: Boolean = true,
    var suggestToAssociateTidalFileTypeWithHaskell: Boolean = true
) : PersistentStateComponent<Settings> {
    override fun getState() = this

    override fun loadState(state: Settings) {
        XmlSerializerUtil.copyBean(state, this)
    }
}