package tidal.settings

import com.intellij.openapi.components.service
import com.intellij.openapi.options.SearchableConfigurable
import javax.swing.JComponent

class TidalConfigurable: SearchableConfigurable {
    private lateinit var settingsForm: SettingsForm

    override fun createComponent(): JComponent {
        settingsForm = SettingsForm(service())
        return settingsForm.root
    }

    override fun apply() {
        settingsForm.applyChanges()
    }

    override fun reset() {
        settingsForm.resetChanges()
    }

    override fun isModified() = settingsForm.isModified()

    override fun getDisplayName() = "Tidal Cycles"

    override fun getId() = "Tidal Cycles"
}
