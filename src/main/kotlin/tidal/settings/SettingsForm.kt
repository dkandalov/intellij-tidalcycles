package tidal.settings

import com.intellij.ide.BrowserUtil
import com.intellij.openapi.fileChooser.FileChooserDescriptor
import com.intellij.openapi.ui.TextComponentAccessor
import com.intellij.openapi.ui.TextFieldWithBrowseButton
import com.intellij.openapi.util.Ref
import com.intellij.ui.DocumentAdapter
import com.intellij.ui.components.JBCheckBox
import com.intellij.ui.components.JBTextField
import com.intellij.ui.components.labels.LinkLabel
import javax.swing.JPanel
import javax.swing.event.ChangeListener
import javax.swing.event.DocumentEvent

class SettingsForm(private val initialState: Settings) {
    lateinit var root: JPanel

    @Suppress("unused")
    private lateinit var settingsPanel: JPanel
    private lateinit var ghciPath: TextFieldWithBrowseButton
    private lateinit var tidalBootScriptPath: TextFieldWithBrowseButton
    private lateinit var logEvaluatedCodeCheckBox: JBCheckBox
    private lateinit var openReadme: LinkLabel<Void>

    private var isUpdating = Ref(false)
    private val currentState = Settings()

    init {
        currentState.loadState(initialState)
        updateUIFromState()

        val commonDocumentListener = object : DocumentAdapter() {
            override fun textChanged(e: DocumentEvent) = noReentryWhen(isUpdating) { updateStateFromUI() }
        }
        val changeListener = ChangeListener {
            noReentryWhen(isUpdating) { updateStateFromUI() }
        }

        ghciPath.addBrowseFolderListener(
            "GHCI Path", null, null,
            FileChooserDescriptor(true, false, false, false, false, false),
            TextComponentAccessor.TEXT_FIELD_WHOLE_TEXT
        )
        tidalBootScriptPath.addBrowseFolderListener(
            "Tidal Boot Script Path", null, null,
            FileChooserDescriptor(true, false, false, false, false, false),
            TextComponentAccessor.TEXT_FIELD_WHOLE_TEXT
        )
        (tidalBootScriptPath.textField as? JBTextField)?.emptyText?.text = "BootTidal.hs bundled with plugin"

        ghciPath.textField.document.addDocumentListener(commonDocumentListener)
        tidalBootScriptPath.textField.document.addDocumentListener(commonDocumentListener)
        logEvaluatedCodeCheckBox.addChangeListener(changeListener)
        openReadme.setListener(
            { _, _ -> BrowserUtil.open("https://gitlab.com/dkandalov/intellij-tidalcycles") },
            null
        )
    }

    private fun updateUIFromState() {
        ghciPath.textField.text = currentState.ghciPath
        tidalBootScriptPath.textField.text = currentState.tidalBootScriptPath
        logEvaluatedCodeCheckBox.isSelected = currentState.logEvaluatedCode
    }

    private fun updateStateFromUI() {
        currentState.ghciPath = ghciPath.textField.text
        currentState.tidalBootScriptPath = tidalBootScriptPath.textField.text
        currentState.logEvaluatedCode = logEvaluatedCodeCheckBox.isSelected
    }

    fun applyChanges(): Settings {
        initialState.loadState(currentState)
        return initialState
    }

    fun resetChanges() {
        currentState.loadState(initialState)
        noReentryWhen(isUpdating) {
            updateUIFromState()
        }
    }

    fun isModified() = currentState != initialState

    private inline fun noReentryWhen(ref: Ref<Boolean>, f: () -> Unit) {
        if (!ref.get()) {
            ref.set(true)
            f()
            ref.set(false)
        }
    }
}