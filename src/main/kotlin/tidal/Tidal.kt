package tidal

import com.intellij.execution.configurations.GeneralCommandLine
import com.intellij.execution.configurations.GeneralCommandLine.ParentEnvironmentType.CONSOLE
import com.intellij.execution.process.ProcessNotCreatedException
import com.intellij.notification.Notification
import com.intellij.notification.NotificationAction
import com.intellij.notification.NotificationType.INFORMATION
import com.intellij.notification.Notifications
import com.intellij.openapi.Disposable
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.application.runInEdt
import com.intellij.openapi.application.runWriteAction
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.fileTypes.FileTypeManager
import com.intellij.openapi.fileTypes.UnknownFileType
import tidal.settings.Settings
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.util.*

@Service
class Tidal(
    private val settings: Settings = service(),
    private val console: Console = service<IdeConsole>(),
    private val ghci: Ghci = GhciImpl(
        onStdOut = { runInEdt { console.logInfo(it) } },
        onStdErr = { runInEdt { console.logError(it) } },
        onInput = { runInEdt { console.logUserInput(it) } }
    )
) : Disposable {
    private val mutedConnections = TreeSet<Int>()

    init {
        val fileTypeManager = FileTypeManager.getInstance()
        val haskellFileType = fileTypeManager.getFileTypeByExtension("hs")
        val tidalFileType = fileTypeManager.getFileTypeByExtension("tidal")
        if (settings.suggestToAssociateTidalFileTypeWithHaskell &&
            haskellFileType != UnknownFileType.INSTANCE && tidalFileType == UnknownFileType.INSTANCE
        ) {
            val message = "Associate \".tidal\" files with Haskell?"
            val notification = Notification("Tidal", "Tidal", message, INFORMATION)
                .addAction(object : NotificationAction("Apply") {
                    override fun actionPerformed(event: AnActionEvent, notification: Notification) {
                        runWriteAction {
                            fileTypeManager.associateExtension(haskellFileType, "tidal")
                            notification.expire()
                        }
                    }
                })
                .addAction(object : NotificationAction("Don't show again") {
                    override fun actionPerformed(event: AnActionEvent, notification: Notification) {
                        settings.suggestToAssociateTidalFileTypeWithHaskell = false
                        notification.expire()
                    }
                })
            ApplicationManager.getApplication().messageBus.syncPublisher(Notifications.TOPIC).notify(notification)
        }
    }

    fun start() = withExceptionHandler {
        console.logInfo("Starting TidalCycles")
        ghci.start { GeneralCommandLine(settings.ghciPath).withParentEnvironmentType(CONSOLE).createProcess() }
        loadTidalBootScript(settings.tidalBootScriptPath).split("\n")
            .forEach { ghci.writeLine(it) }
    }

    fun isRunning() = ghci.isRunning()

    fun evaluate(code: String) = withExceptionHandler {
        code.replace("\t", "  ")
            .replace(Regex("\n +"), "\r  ")
            .split("\n")
            .forEach { line ->
                ghci.writeLine(line)
            }
    }

    fun hush() = withExceptionHandler {
        val command = "hush"
        ghci.writeLine(command)
    }

    fun toggleMute(connection: Int) = withExceptionHandler {
        val isMuted = connection in mutedConnections
        val command =
            if (isMuted) {
                mutedConnections.remove(connection)
                "unmute $connection"
            } else {
                mutedConnections.add(connection)
                "mute $connection"
            }
        ghci.writeLine(command)
    }

    fun unmuteAll() = withExceptionHandler {
        mutedConnections.forEach { toggleMute(it) }
    }

    fun stop() = withExceptionHandler {
        console.logInfo("Stopping TidalCycles")
        ghci.stop()
    }

    override fun dispose() = stop()

    private fun loadTidalBootScript(path: String): String {
        return if (path.isBlank()) javaClass.classLoader.getResourceAsStream("BootTidal.hs")!!.bufferedReader().readText()
        else {
            val file = File(path)
            if (file.exists()) file.readText() else throw FileNotFoundException("File doesn't exist $path")
        }
    }

    private inline fun withExceptionHandler(f: () -> Unit) =
        try {
            f()
        } catch (e: IOException) {
            console.logError(e.message ?: e.toString())
        } catch (e: ProcessNotCreatedException) {
            console.logError(e.message ?: e.toString())
        } catch (e: Exception) {
            console.logException(e)
        }
}
