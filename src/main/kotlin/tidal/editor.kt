package tidal

import com.intellij.concurrency.JobScheduler
import com.intellij.openapi.application.invokeLater
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.colors.EditorColors
import com.intellij.openapi.editor.markup.HighlighterLayer
import com.intellij.openapi.editor.markup.HighlighterTargetArea
import com.intellij.openapi.util.TextRange
import java.util.concurrent.TimeUnit.MILLISECONDS


fun Editor.selectionTextAndRange(): Pair<String, TextRange>? {
    val textRange = TextRange(selectionModel.selectionStart, selectionModel.selectionEnd)
    return if (textRange.isEmpty) null else Pair(document.getText(textRange), textRange)
}

fun Editor.currentLineTextAndRange(): Pair<String, TextRange>? {
    val line = caretModel.logicalPosition.line
    val textRange = TextRange(document.getLineStartOffset(line), document.getLineEndOffset(line))
    return if (textRange.isEmpty) null else Pair(document.getText(textRange), textRange)
}

fun Editor.currentParagraphTextAndRange(): Pair<String, TextRange>? {
    val currentLine = caretModel.logicalPosition.line
    val lines = document.text.split("\n")
    val lastLine = lines.size - 1
    if (lines[currentLine].isEmpty()) return null

    var fromLine = (0..currentLine).reversed().find { lines[it].isEmpty() } ?: 0
    val toLine = (currentLine..lastLine).find { lines[it].isEmpty() } ?: lastLine

    if (lines[fromLine].isEmpty() && fromLine < lastLine) fromLine++

    val textRange = TextRange(document.getLineStartOffset(fromLine), document.getLineEndOffset(toLine))
    return if (textRange.isEmpty) null else Pair(document.getText(textRange), textRange)
}

fun Editor.highlight(textRange: TextRange) {
    val highlighter = markupModel.addRangeHighlighter(
        EditorColors.SEARCH_RESULT_ATTRIBUTES,
        textRange.startOffset,
        textRange.endOffset,
        HighlighterLayer.LAST,
        HighlighterTargetArea.EXACT_RANGE
    )
    val command = { invokeLater { markupModel.removeHighlighter(highlighter) } }
    JobScheduler.getScheduler().schedule(command, 200, MILLISECONDS)
}
