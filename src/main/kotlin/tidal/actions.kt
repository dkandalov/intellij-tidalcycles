package tidal

import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys.EDITOR
import com.intellij.openapi.actionSystem.CommonDataKeys.VIRTUAL_FILE
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.util.TextRange

class StartOrStopTidal : AnAction(), DumbAware {
    override fun actionPerformed(event: AnActionEvent) {
        val console = service<IdeConsole>()
        val tidal = service<Tidal>()
        if (tidal.isRunning()) {
            tidal.stop()
            console.stop()
        } else {
            console.start(event.project)
            tidal.start()
        }
    }

    override fun update(event: AnActionEvent) {
        event.presentation.text = if (service<Tidal>().isRunning()) "Stop Tidal" else "Start Tidal"
    }
}

class EvaluateLine : EvaluateAction("Evaluate Line") {
    override fun textAndRangeToEvaluate(editor: Editor) =
        editor.selectionTextAndRange() ?: editor.currentLineTextAndRange()
}

class EvaluateParagraph : EvaluateAction("Evaluate Paragraph") {
    override fun textAndRangeToEvaluate(editor: Editor) =
        editor.selectionTextAndRange() ?: editor.currentParagraphTextAndRange()
}

abstract class EvaluateAction(text: String) : AnAction(text), DumbAware {
    abstract fun textAndRangeToEvaluate(editor: Editor): Pair<String, TextRange>?

    override fun actionPerformed(event: AnActionEvent) {
        val editor = event.getData(EDITOR) ?: return
        val (text, textRange) = textAndRangeToEvaluate(editor) ?: return
        editor.highlight(textRange)
        event.getRunningTidal().evaluate(text)
    }

    override fun update(event: AnActionEvent) {
        event.presentation.isEnabled = event.getData(VIRTUAL_FILE)?.extension == "tidal"
    }
}

class HushTidal : AnAction("Hush"), DumbAware {
    override fun actionPerformed(event: AnActionEvent) = service<Tidal>().hush()
    override fun update(event: AnActionEvent) {
        event.presentation.isEnabled = service<Tidal>().isRunning()
    }
}

abstract class ToggleMute(private val connection: Int) : AnAction("Toggle Mute $connection"), DumbAware {
    override fun actionPerformed(event: AnActionEvent) = event.getRunningTidal().toggleMute(connection)
    override fun update(event: AnActionEvent) {
        event.presentation.isEnabled = service<Tidal>().isRunning()
    }
}

class ToggleMute1 : ToggleMute(1)
class ToggleMute2 : ToggleMute(2)
class ToggleMute3 : ToggleMute(3)
class ToggleMute4 : ToggleMute(4)
class ToggleMute5 : ToggleMute(5)
class ToggleMute6 : ToggleMute(6)
class ToggleMute7 : ToggleMute(7)
class ToggleMute8 : ToggleMute(8)
class ToggleMute9 : ToggleMute(9)
class ToggleMute10 : ToggleMute(10)

class UnmuteAll : AnAction("Unmute All"), DumbAware {
    override fun actionPerformed(event: AnActionEvent) = event.getRunningTidal().unmuteAll()
}

private fun AnActionEvent.getRunningTidal(): Tidal {
    val tidal = service<Tidal>()
    if (!tidal.isRunning()) {
        StartOrStopTidal().actionPerformed(this)
    }
    return tidal
}
