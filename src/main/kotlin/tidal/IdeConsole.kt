package tidal

import com.intellij.application.subscribe
import com.intellij.execution.filters.TextConsoleBuilderFactory
import com.intellij.execution.ui.ConsoleView
import com.intellij.execution.ui.ConsoleViewContentType
import com.intellij.execution.ui.ConsoleViewContentType.*
import com.intellij.openapi.Disposable
import com.intellij.openapi.components.Service
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.ProjectManager
import com.intellij.openapi.project.ProjectManagerListener
import com.intellij.openapi.util.Disposer
import com.intellij.openapi.wm.RegisterToolWindowTask
import com.intellij.openapi.wm.ToolWindowManager

interface Console {
    fun logInfo(message: String)
    fun logError(message: String)
    fun logUserInput(message: String)
    fun logException(throwable: Throwable)
}

@Service
class IdeConsole : Console, Disposable {
    private val tidalToolWindowId = "TidalCycles"
    private var consoleByProject = HashMap<Project, ConsoleView>()
    private var disposable: Disposable? = null

    fun start(project: Project?) {
        disposable = Disposer.newDisposable()
        ProjectManager.TOPIC.subscribe(disposable, object : ProjectManagerListener {
            override fun projectOpened(project: Project) = registerTidalToolWindowIn(project)
            override fun projectClosed(project: Project) {
                consoleByProject.remove(project)
            }
        })
        ProjectManager.getInstance().openProjects.forEach { registerTidalToolWindowIn(it) }
        if (project != null) {
            ToolWindowManager.getInstance(project).getToolWindow(tidalToolWindowId)?.show()
        }
    }

    private fun registerTidalToolWindowIn(project: Project) {
        val toolWindowManager = ToolWindowManager.getInstance(project)
        if (toolWindowManager.getToolWindow(tidalToolWindowId) == null) {
            val console = TextConsoleBuilderFactory.getInstance().createBuilder(project).console
            toolWindowManager.registerToolWindow(RegisterToolWindowTask(tidalToolWindowId, component = console.component, canCloseContent = false))
            consoleByProject[project] = console
        }
    }

    fun stop() {
        disposable?.let { Disposer.dispose(it) }
        consoleByProject.keys.forEach { project ->
            @Suppress("DEPRECATION")
            ToolWindowManager.getInstance(project).unregisterToolWindow(tidalToolWindowId)
        }
        consoleByProject.clear()
    }

    override fun dispose() = stop()

    override fun logInfo(message: String) {
        printToConsoles("$message\n", NORMAL_OUTPUT)
    }

    override fun logUserInput(message: String) {
        printToConsoles("$message\n", USER_INPUT)
    }

    override fun logError(message: String) {
        printToConsoles("$message\n", ERROR_OUTPUT)
    }

    override fun logException(throwable: Throwable) {
        printToConsoles("${throwable.stackTraceToString()}\n", ERROR_OUTPUT)
    }

    private fun printToConsoles(s: String, contentType: ConsoleViewContentType) {
        consoleByProject.values.forEach { it.print(s, contentType) }
    }
}