package tidal

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.io.PipedInputStream
import java.io.PipedOutputStream
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class GhciTest {
    private val process = TestProcess()

    @Test internal fun `should write line to process with a line end`() {
        val ghci = GhciImpl({}, {}, {})
        ghci.start { process }

        ghci.writeLine("test line")

        val bytes = process.writeInput.readNBytes(10)
        assertEquals(String(bytes), "test line\n")
    }

    @Test internal fun `should invoke stdOut listener on stdOut text output`() {
        val latch = CountDownLatch(1)
        val message = StringBuffer()
        val ghci = GhciImpl({ latch.countDown(); message.append(it) }, {}, {})
        ghci.start { process }

        process.stdOut.write("Test".toByteArray())

        assertTrue(latch.await(1, TimeUnit.SECONDS))
        assertEquals(message.toString(), "Test")
    }

    @Test internal fun `should invoke stdErr listener on stdErr text output`() {
        val latch = CountDownLatch(1)
        val message = StringBuffer()
        val ghci = GhciImpl({}, { latch.countDown(); message.append(it) }, {})
        ghci.start { process }

        process.stdErr.write("Test".toByteArray())

        assertTrue(latch.await(1, TimeUnit.SECONDS))
        assertEquals(message.toString(), "Test")
    }

    @Test internal fun `should considered running if it's not yet stopped`() {
        val ghci = GhciImpl({}, {}, {})
        ghci.start { process }

        assertTrue(ghci.isRunning())

        ghci.stop()

        assertFalse(ghci.isRunning())
    }

    @Test internal fun `should throw exception on writeLine if stopped`() {
        val ghci = GhciImpl({}, {}, {})
        ghci.start { process }

        ghci.stop()

        assertThrows<IOException> { ghci.writeLine("test") }
    }

    private class TestProcess : Process() {
        private val outInput = PipedInputStream()
        val stdOut = PipedOutputStream(outInput)

        private val errInput = PipedInputStream()
        val stdErr = PipedOutputStream(errInput)

        private val writeOutput = PipedOutputStream()
        val writeInput = PipedInputStream(writeOutput)

        var destroyed = false

        override fun getOutputStream(): OutputStream = writeOutput
        override fun getInputStream(): InputStream = outInput
        override fun getErrorStream(): InputStream = errInput

        override fun waitFor(): Int = 0

        override fun exitValue(): Int {
            if (destroyed) {
                return -1
            } else {
                throw IllegalThreadStateException("not exited")
            }
        }

        override fun destroy() {
            outInput.close()
            stdOut.close()
            errInput.close()
            stdErr.close()
            writeInput.close()
            writeInput.close()
            destroyed = true
        }

    }

}