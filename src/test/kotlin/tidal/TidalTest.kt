package tidal

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import tidal.settings.Settings

class TidalTest {

    @Test
    fun `should start and stop ghci and print logs`() {
        val console = InMemoryConsole()
        val ghci = FakeGhci()
        val tidal = Tidal(Settings(), console, ghci)

        tidal.start()
        assertTrue(tidal.isRunning())

        tidal.stop()
        assertFalse(tidal.isRunning())

        assertTrue(console.infos.contains("Starting TidalCycles"))
        assertTrue(console.infos.contains("Stopping TidalCycles"))
        assertTrue(console.errors.isEmpty())
    }
}

private class FakeGhci: Ghci {
    var running = false

    override fun start(createProcess: () -> Process) {
        running = true
    }

    override fun writeLine(line: String) {
    }

    override fun stop() {
        running = false
    }

    override fun isRunning(): Boolean = running

}

private class InMemoryConsole : Console {
    val infos = ArrayList<String>()
    val errors = ArrayList<String>()

    override fun logInfo(message: String) {
        infos.add(message)
    }

    override fun logUserInput(message: String) {
    }

    override fun logError(message: String) {
        errors.add(message)
    }

    override fun logException(throwable: Throwable) {
    }
}
