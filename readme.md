# TidalCycles plugin for IntelliJ IDEs
[Tidal Cycles](https://tidalcycles.org) (or just Tidal for short) is software for live coding music.
This plugin allows you to interact with Tidal from IDEs based IntelliJ platform.

### How to use the plugin
- Install Tidal (see [installation instructions](https://tidalcycles.org/docs)).
- Start tidal via `Main Menu -> Tools -> Tidal Cycles -> Start Tidal`.
- You should see the Tidal tool window with output from GHCI running Tidal.
  If everything is set up correctly, it should contain the "Connected to SuperDirt" line.
- Open a ".tidal" file, or create one, for example, with `d1 $ sound "bd hh sn hh"`. 
  Use `Evaluate Line` or `Evaluate Paragraph` actions to play the code using Tidal. 
  You should hear some sounds! 🔊
- You can use `Hush`, `Toggle Mute` and `Unmute All` actions.

Note that even though plugin actions have default keyboard shortcuts, 
you can always change them to something that suits you 
(that is, if you manage to find a non-conflicting shortcut, or maybe remove shortcuts from other IDE actions).

To configure the path to GHCI and Tidal boot script see `IDE Preferences -> Other Settings -> Tidal Cycles`.

IntelliJ IDEs has basic syntax highlighting for Haskell files, 
so Tidal plugin will suggest on startup to associate `.tidal` files with Haskell file type.


### Contributing
All contributions are welcome. You can build the project using [gradle-intellij-plugin](https://github.com/JetBrains/gradle-intellij-plugin),
e.g. `gradle buildPlugin` or `gradle runIde`.
